import React from 'react'
import {HiUsers} from 'react-icons/hi'
import {ImLocation2} from 'react-icons/im'
import {FaNetworkWired} from 'react-icons/fa'
import {MdOutlineWork} from 'react-icons/md'
import {MdMarkunreadMailbox} from 'react-icons/md'
import {SiAdobeindesign} from 'react-icons/si'
import {FaResearchgate} from 'react-icons/fa'


export default function Sidebar() {
  return (
    <div>
      <aside class="main-sidebar sidebar-dark-primary ">
   

  
    <div class="sidebar">
   
     
      <div class="user-panel mt-2 pb-2 mb-2 w-1 ">
        <div class="image">
          <img src="dist/img/lyfa.jpg" class="img-circle elevation-2" style={{borderRaduis:10}} alt="User Image"/>
          <a href="#" class="d-block">Faly Seck</a>
        </div>
      </div>
     
    

  
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column w-10"  >
        
        
        
          <li class="nav-item ">
          
            <ul class="nav nav-treeview">
            
            

            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="fa fa-home nav-icon" ></i>
              <p>
                HOME
                
              </p>
            </a>
          
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="fa fa-briefcase nav-icon"></i>
              <p>
              JOBS
                
              </p>
            </a>
         
          </li>
          <li class="nav-item has-treeview">
          <a href="pages/calendar.html" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendar
              
              </p>
            </a>
            <ul class="nav nav-treeview">
             
           
            
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
              
              </p>
            </a>
            <ul class="nav nav-treeview">
             
           
             
            </ul>
          </li>
         
          <li class="nav-item">
            <a href="pages/calendar.html" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendar
              
              </p>
            </a>
          </li>
          <li class="nav-item">
          
          </li>
          <li class="nav-item has-treeview">
           
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/mailbox/mailbox.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inbox</p>
                </a>
              </li>
              
            
            </ul>
          </li>
          <li class="nav-item has-treeview">
           
          
          </li>
          <li class="nav-item has-treeview">
          
            <ul class="nav nav-treeview">
           
              <li class="nav-item">
                <a href="pages/examples/register.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Register</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/forgot-password.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Forgot Password</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/recover-password.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Recover Password</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/lockscreen.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lockscreen</p>
                </a>
              </li>
              
           
             
             
             
            </ul>
          </li>
         
        <hr/>
          <li class="nav-header">Admin</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
           < HiUsers className='nav-icon'/>
              <p class="text">Users</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <ImLocation2 class="nav-icon "/>
              <p>Localisation</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <FaNetworkWired class="nav-icon "/>
              <p>Job </p>
            </a>
          </li>


          <li class="nav-item">
            <a href="#" class="nav-link">
              <MdOutlineWork class="nav-icon "/>
              <p>Informational</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <MdOutlineWork class="nav-icon "/>
              <p>Social</p>
            </a>
          </li>
          

          <li class="nav-header">Categories</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
            <MdMarkunreadMailbox  class="nav-icon "style={{color:'yellow'}}/>
              <p class="text">Administrative</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
           <SiAdobeindesign class="nav-icon " style={{color:'royalblue'}}/>
              <p>Design</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
           < FaResearchgate class="nav-icon " style={{color:'red'}}/>
              <p>Humain Ressource</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
           < FaResearchgate class="nav-icon " style={{color:'green'}}/>
              <p>Humain Ressource</p>
            </a>
          </li>
          
        </ul>
      </nav>
     
    </div>
   
  </aside>
    </div>
  )
}
