import React from 'react';
import '../Components/Progres.css'

export default function Progres() {
  return (
    <div className='container d-flex flex-wrap d-flex justify-content-between'>
      <div className=' col-lg-4 col-sm-12'>
        <div className='box'>
          <div className='percent'>

            <svg>
              <circle cx='70' cy='70' r="70" ></circle>
              <circle cx='70' cy='70' r="70" ></circle>
            </svg>     <div className='number'>
              <h2>3.154</h2>
            </div>
          </div>
        </div>
      </div>

      <div className='  col-lg-4 col-sm-12'>
        <div className='box1'>      
         <div className='percent'>
          <svg>
            <circle cx='70' cy='70' r="70" ></circle>
            <circle cx='70' cy='70' r="70" ></circle>
          </svg>
          <div className='number'>
            <h2>1.546</h2>
          </div>
        </div>

        </div>
      </div>
      <div className=' col-lg-4 col-sm-12'>
        <div className='box2'>      
         <div className='percent'>
          <svg >
            <circle cx='70' cy='70' r="70" ></circle>
            <circle cx='70' cy='70' r="70" ></circle>
          </svg>
          <div className='number'>
            <h2>912</h2>
          </div>
        </div>
       
       </div>
       <div className='button '>
        <a href="#" class="btn btn-primary text-center" style={{background:'aliceblue',borderRadius:15,color:'black'}}>TOTAL : out of 3.125 uers</a>
        </div>
      </div>
     
    </div>

  )
}
