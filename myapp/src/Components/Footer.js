import React from 'react'
import {FaFacebookF} from 'react-icons/fa'
import {BsTwitter} from 'react-icons/bs'
import {MdLocalPostOffice} from 'react-icons/md'
import {RiLinkUnlink} from 'react-icons/ri'
import {RiLinkedinBoxLine}  from'react-icons/ri'
import {FaUserCircle} from 'react-icons/fa'
import {MdUpload} from 'react-icons/md'
import {FiEdit2} from 'react-icons/fi'

export default function Footer() {
  return (
<div class="row m-4">
  <div class="col-sm-8">
  <div className='' >
        <div class="card " style={{marginLeft:60, border:'none'}}>
  <div class="card-header" style={{background:'gray'}}>
    <ul class="nav nav-tabs card-header-tabs" >
  
      <li class="nav-item">
        <a class="nav-link" href="#" style={{color:'white'}}>JOB DESCRIPTION</a>
      </li>
      <li class="nav-item">
      <button type="button" class="btn btn-info" style={{background:'' ,color:'white'}}><FiEdit2/>EDIT</button>
      </li>
    </ul>
  </div>
  <div class="card-body">
  <table class="table caption-top">
 
  
  <tbody>
    <tr style={{height:100, textAlign:'center'}}>
      <th scope="row">Localisation</th>
      <td>Senegal,Dakar</td>
     
    </tr>
    <tr style={{height:100, textAlign:'center'}}>
      <th scope="row">Employment type</th>
      <td>Full Time</td>
     
    </tr>
    <tr style={{height:100, textAlign:'center'}}>
      <th scope="row">Experience</th>
      <td>Mid-level</td>
      
    </tr>

    <tr style={{height:200, textAlign:'center'}}>
      <th scope="row">Description</th>
      <td>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', </td>
      
    </tr>
    <tr style={{height:100, textAlign:'center'}}>
      <th scope="row">AproTRp </th>
      <td>Tome Tizz</td>
      
    </tr>

    <tr style={{height:100, textAlign:'center'}}>
      <th scope="row">Approved Salary</th>
      <td>500000CFA</td>
      
    </tr>
  </tbody>
</table>
  </div>
</div>
    </div>
   
  </div>
  <div class="col-sm-4">
    
  <div class="card" style={{backgroundColor:'blueviolet'}}>
  <div class="card-body text-white d-flex justify-content-between">
  <FaFacebookF style={{color:'white'}}/>Post Facebook
  </div>
</div>

<div class="card"  style={{backgroundColor:'darkcyan'}}>

  <div class="card-body text-white d-flex justify-content-between">
  <BsTwitter style={{color:'white'}}/>Post Twitter
  </div>
</div>

<div class="card" style={{backgroundColor:'royalblue'}}>
  <div class="card-body text-white d-flex justify-content-between">
 < RiLinkedinBoxLine style={{color:'white'}}/>post Linkdin
 {/* <p className='para mb-3' style={{color:'white',textAlign:'center'}}>post linkdin</p> */}
  </div>
</div>
<div class="card-body  d-flex " >
        <RiLinkUnlink  style={{fontSize:25}}/><h3>Link to this job</h3>
       
    </div>
    <div>
    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece</p>
    </div>

    <div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="https://username" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
  <span class="input-group-text" id="basic-addon2" style={{background:'mediumseagreen'}}>Copy</span>
</div>
<br/>
<hr></hr>
<div class="card-body  d-flex " >
        <FaUserCircle style={{fontSize:25}}/><h3>Link to this job</h3>
       
    </div>
<div>
    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece</p>
    </div>

    <div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
  <span class="input-group-text" id="basic-addon2" style={{background:'mediumseagreen'}}>Copy</span>
</div>
<br/>
<hr></hr>
<div class="card-body  d-flex " >
<MdUpload style={{fontSize:25}}/><h3>Link to this job</h3>
</div>
<div>
    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece</p>
    </div>
    <input type="text" class="form-control" placeholder="https://username" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
    </div>
    
   
  </div>



    

  )
}
