import React from 'react'
import {BsSearch} from 'react-icons/bs'

export default function Nav() {
  return (
    <div>
   <nav class="main-header navbar navbar-expand navbar-white navbar-light">
   
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    
    </ul>
    <div class="input-group mb-3">
  <span class="input-group-text" id="basic-addon1">  <BsSearch/></span>
  <input type="text" class="form-control" placeholder="Search your job" aria-label="Username" aria-describedby="basic-addon1"/>
</div>


    {/* <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <div class="input-group-append">
         <input />
           <BsSearch/>
           
        </div>
      </div>
    </form> */}

  
    <ul class="navbar-nav ml-auto">
  
      <li class="nav-item dropdown">
        
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">

          </a>
          <div class="dropdown-divider"></div>
         
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
   
      <li class="nav-item dropdown">
       
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
         
        
         
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
        <i class="fa fa-user-plus" aria-hidden="true"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
     
  </div>
   
  )
}
