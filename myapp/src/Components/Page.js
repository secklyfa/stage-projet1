import React from 'react'
import Home from '../Components/Home'
import Progres from './Progres'

export default function Page() {
  return (
    
    <div class="row m-4 ">
  <div class="col-sm-8">
    <div class="card ">
      <div class="card-body">
  <Progres/>
  
      </div>
      
    </div>
    
  </div>
  <div class="col-sm-4">
    <div class="card"  >
      <div class="card-body" >
       <Home/>
      </div>
    </div>
  </div>

</div>
    
  )
}
